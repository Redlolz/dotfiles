set number
syntax on
filetype on
colorscheme gruvbox
set background=dark
highlight Comment cterm=bold 

set tabstop=4
set shiftwidth=4
filetype plugin indent on

let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

autocmd FileType python nnoremap <buffer> <F5> :update<bar>!python %<CR>
autocmd FileType sh nnoremap <buffer> <F5> :update<bar>!sh %<CR>
autocmd FileType rust nnoremap <buffer> <F5> :update<bar>!cargo run<CR>
autocmd FileType rust nnoremap <buffer> <F6> :update<bar>!cargo build<CR>

autocmd FileType markdown map <F5> :!pandoc<space><C-r>%<space>-o<space><C-r>%.pdf<Enter><Enter>
autocmd FileType markdown map <F5> :!pandoc<space><C-r>%<space>-o<space><C-r>%.html<Enter><Enter>
autocmd FileType rmd map <F5> :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter>
