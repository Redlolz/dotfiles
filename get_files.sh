#!/bin/bash

# Xresources
cp ~/.Xresources ./Xresources

# Fluxbox directory
cp -r ~/.fluxbox ./fluxbox

# Vim directories
cp ~/.vimrc ./vimrc
cp -r ~/.vim ./vim

# Commiting and pushing
git add .
git commit
git push
